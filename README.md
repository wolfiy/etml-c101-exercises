# C101 - Web statique
Répertoire contenant les exercices du cours C101 de l'ETML (ancienne ordonnance). Une version live du code peut être trouvée sur les *pages* du répertoire: https://wolfiy.gitlab.io/etml-c101-exercises/

## Structure du répertoire
Tout le code source se trouve dans le dossier `src`. Celui-ci est décomposé en un dossier pour chaque types de fichiers (html, css, etc.), eux-même décomposés en dossiers contenant les exercices. Par exemple, le contenu de l'exercice 1 (bases d'HTML) est réparti ainsi:

```
src/html/01/index.html // Index des questions
src/html/01/01.html    // Question 1
src/html/01/02.html    // Question 2
src/css/01/06.css      // Question 6
...
```

A noter que la structure de ce répertoire s'inspire mais **ne suit pas les normes de l'école**.
